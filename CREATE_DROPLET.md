Creating a Remote Compute Environment on Digital Ocean
------------------------------------------------------

Now that you understand how to use Vagrant to create a virtual machine on your laptop, use the VM to create a remote compute environment on Digital Ocean.

Install Digital Ocean doctl utility
-----------------------------------

Refer to the documentation provided at: https://docs.digitalocean.com/reference/doctl/how-to/install/ and install the `doctl` command line utility.

```shell
cd ~
wget https://github.com/digitalocean/doctl/releases/download/v1.60.0/doctl-1.60.0-linux-amd64.tar.gz
tar xf ~/doctl-1.60.0-linux-amd64.tar.gz
sudo mv ~/doctl /usr/local/bin
```

Verify the program is installed successfully:

```shell
doctl -h
```

Create a personal access token: https://docs.digitalocean.com/reference/api/create-personal-access-token/
and save it in your copy buffer or in a file. Initialize the token.

```shell
doctl auth init --context foo
``` 

> Note: to verify the token is valid, issue `doctl account get`.

You will need an SSH key associate with your Digital Ocean account. If you have not already generated a key and associated it with your account, follow these instructions: https://docs.digitalocean.com/products/droplets/how-to/add-ssh-keys/create-with-openssh/


> Note: Generate this key from the system you will use to access the droplet created in the next step.

Create a Remote Compute instance
--------------------------------
You can create a DO Droplet using the following command example:

```shell
doctl compute droplet create --region nyc1  --image ubuntu-18-04-x64 --size s-1vcpu-1gb --ssh-keys 13:8a:3c:ae:c1:ec:af:33:2a:7c:3a:0a:ee:a4:78:e2 vagrant-test
```
The value shown for `--ssh-keys` is the fingerprint of the SSH key.

List the droplets to learn the public IP address associated with the remote compute instance.

```shell
doctl compute droplet list
```

Now configure VS Code to use this instance. When finished either delete the instance via the web interface, or issue `doctl compute droplet delete <id>`, where `<id>` is learned from the `doctl compute droplet list` command output.

Author
------
Joel W. King @joelwking
