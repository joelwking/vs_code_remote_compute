vs_code_remote_compute
----------------------

__Instructions for using VS Code Remote Explorer for a remote compute environment (Cloud compute, VM, etc.)__

These instructions assume you have previously installed:

* A supported SSH client, Refer to [Installing a supported SSH client]https://code.visualstudio.com/docs/remote/troubleshooting#_installing-a-supported-ssh-client).
* Visual Studio Code, [Download and install it](https://code.visualstudio.com/download) and the [Visual Studio Code Remote Development Extension Pack](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack).

Remote Compute Environment
--------------------------
You will need a remote compute environment and SSH keys associated with the compute environment. You can create a remote compute environment using a [Digital Ocean](https://www.digitalocean.com/) Droplet, use the [instructions](https://docs.digitalocean.com/products/droplets/how-to/add-ssh-keys/) for creating and associating SSH keys to a Droplet.


If you have followed the examples for creating a Vagrant virtual machine in https://gitlab.com/joelwking/vs_code_vagrant you can use that VM and these instructions `./CREATE_DROPLET.md`.

Other cloud providers have different methods of managing SSH keys, follow the instructions of your target compute environment.

Linux | macOS SSH client
------------------------
Using macOS or Linux, you should not need to install an additional ssh client. You can locate the path to your ssh client using the command `which ssh`. The absolute path for the ssh executable will likely be `/usr/bin/ssh`. Review the Windows instructions below if you wish to verify or change the value for `remote.ssh.path`.

Windows
-------
Installing Git for Windows is recommended. Refer to https://git-scm.com/download/win. 

Once installed, determine the absolute path of the `ssh` executable. It may be at location `c:\Program Files\Git\usr\bin\ssh.exe`.

Launch Visual Studio Code, in the lower left of the window, right click the gear icon and select `Settings`.  

In the search settings dialog box, type `remote.ssh.path`. In the resulting dialog box, enter the absolute path of your ssh client. For example ` C:/'Program Files'/Git/usr/bin/ssh.exe`. Note the back slash has been substituted by a forward slash and single quotes were added to bound the directory with embedded space. 

If you select the 'gear' icon next to the `remote.ssh.path` select `Copy Settings as JSON`. The copy buffer will contain:

```
"remote.SSH.path": " C:/\"Program Files\"/Git/usr/bin/ssh.exe"
```

Close the Settings window.

Testing your SSH client (optional)
----------------------------------

Some SSH clients will prevent establishing an SSH connection to the remote machine if the key file is 'group' or 'world' readable, so you don't accidentally share your private key with other users on your system.

You can test connectivity from your laptop to the remote compute environment using your SSH client and the private key file. Using a Droplet, the command would look similar to:

```bash
% ssh -i ~/.ssh/digital_ocean_p4 root@192.0.2.1
```

If the connection fails due to too open file permissions on the key file, you will need to change the permissions so the key file can be read and written by the owner and no permissions assigned to the group or world, e.g. `chmod 600 ~/.ssh/<your_private_key_file>`.

VS Code Remote Explorer
-----------------------

To configure, select the 'Remote Explorer' icon on the left side of the window. Select the 'gear' icon to create a  'custom configuration file'. Provide an absolute path to the configuration file. For example: `C:\Users\kingjoe\.ssh\config` (Windows) or `/Users/kingjoe/.ssh/config` for macOS. 

When selecting the '+' icon next to the SSH TARGETS, you may be prompted for an 'SSH Connection Command', enter `ssh training@example.net` an press enter to confirm your input. It will generate a sample configuration. 

You will need to **Add**  the SSH configuration entry for your remote compute environment in the SSH configuration file. Using the Digital Ocean Droplet as an example, your file `~/.ssh/config `should look similar to the following.

```
Host DigitalOcean_P4
  Hostname 192.0.2.1
  User root
  IdentityFile ~/.ssh/digital_ocean_p4
```

Save your changes to the SSH configuration file.  You can connect to the remote compute environment by selecting the `Connect to host in new window` icon to the right of the hostname (`DigitalOcean_P4`) you specified in the SSH configuration file.

When the new window appears, you may open a new terminal or folder on the remote compute environment and issue commands and edit files.

Author
------

joel.king@wwt.com @joelwking

